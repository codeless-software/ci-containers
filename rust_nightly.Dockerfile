FROM registry.fedoraproject.org/fedora-minimal:37

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH
    
RUN microdnf install -y g++ cmake fontconfig-devel mesa-vulkan-drivers --nodocs --setopt install_weak_deps=0

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path --profile=minimal --default-toolchain nightly
RUN rustup component add miri llvm-tools-preview
RUN cargo install grcov

# Cleanup
RUN rm -rf /usr/local/cargo/registry
RUN microdnf clean all -y
